package com.simon.riemertzon;

public class Main {

    public static void main(String[] args) {
        //1. Lazy initialized singleton
        var lazyInitializedSingleton = RootSingleton1.getInstance();
        System.out.println(lazyInitializedSingleton);

        //2. Public static final Instance field
        var publicStaticFinalInstanceFieldSingleton = RootSingleton2.INSTANCE;
        System.out.println(publicStaticFinalInstanceFieldSingleton);

        //3. Enum
        var enumSingleton = RootSingleton3.INSTANCE;
        System.out.println(enumSingleton);

    }
}
