package com.simon.riemertzon;


public class RootSingleton1 {
    private int UID = 0;
    private int GID = 0;
    private String userName = "root";
    private String name = "Super user";
    private String home = "/root";
    private String shell = "/bin/sh";

    private static RootSingleton1 instance = null;

    public static RootSingleton1 getInstance() {
        if(instance == null) {
            instance = new RootSingleton1();
        }
        return instance;
    }

    @Override
    public String toString(){
        return new StringBuilder(userName)
                .append("(").append(name).append(")")
                .append(" ").append(UID).append(":").append(GID)
                .append(" home: ").append(home).append(" ")
                .append("shell: ").append(shell)
                .toString();
    }

}
